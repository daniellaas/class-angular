import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private TempService: TempService) { }

  likes=0;
  temperature;
  image; 
  city;
  tempData$:Observable<Weather>;
  errorMessage:string; 
  hasError:boolean = false; 


  addLike(){
    this.likes++;
  }

  ngOnInit() {
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.TempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error =>{
        console.log(error.message);
        this.hasError = true; 
        this.errorMessage = error.message; 
      } 
    )
  }
  }


