import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  panelOpenState = false;
  books$:Observable<any>
  id:string;

  constructor(public BooksService:BooksService) { }

  ngOnInit() {
    this.books$ = this.BooksService.getBooks()

  }

  deleteBook(id:string){
    this.BooksService.deleteBook(id);
  }


}
