import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]
  
  constructor(private db:AngularFirestore) { }

  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges(({idField:'id'}));
  }

  getBook(id:string):Observable<any>{
    return this.db.doc(`books/${id}`).get()
  }  

  addBook(title:string, author:string){
    const book = {title:title, author:author}
    this.db.collection('books').add(book);
  }
  
  deleteBook(id:string){
    this.db.doc(`books/${id}`).delete();
  }

  updateBook(id:string, title:string, author:string){
    this.db.doc(`books/${id}`).update({
      title:title,
      author:author
    })
  }


}
