import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  title:string;
  author:string;
  isEdit:boolean = false;
  id:string;
  buttonText:string = 'Add book' ;

  

  constructor(public BooksService:BooksService, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    console.log(this.id);
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Update Book"
      this.BooksService.getBook(this.id).subscribe(
        book => {
          console.log(book.data().author)
          console.log(book.data().title)
          this.author = book.data().author;
          this.title = book.data().title; 
        }
      )
    }


  }

  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.BooksService.updateBook(this.id,this.title,this.author);
    } else {
      this.BooksService.addBook(this.title,this.author)
    }
    this.router.navigate(['/books']);  
  } 

}
