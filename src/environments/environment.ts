// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebase:{
    apiKey: "AIzaSyBJbpf2ePse5_mkX0kO1dulwbVFcEIYki4",
    authDomain: "class-3a8c4.firebaseapp.com",
    databaseURL: "https://class-3a8c4.firebaseio.com",
    projectId: "class-3a8c4",
    storageBucket: "class-3a8c4.appspot.com",
    messagingSenderId: "317616414141",
    appId: "1:317616414141:web:d50a9ed7a39ec8386006d4",
  }
};
 

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
